# Streaming Data System with Kafka Streams  

## Table of contents  
  

### 1. Streaming Data theory  

1.1. What is streaming data  
 * Real time vs streaming processing vs batch processing  
 * Challenges  
 
1.2. Modes of streaming data  
 * Stateless vs stateful  
 * Windows  
 * Event time vs ingestion time vs processing time

1.3. Examples

### 2. Streaming Data tools  

2.1. Apache Kafka  
2.2. Apache Spark (Structured) Streaming  
2.3. Cloud solutions: Amazon Kinesis Streams, Google Dataflow, Azure Stream Analytics  
2.3. Less popular tools: Apache Flink, Apache Flume, Apache Storm, Apache NiFi    

### 3. Project  

3.1. About    
3.2. Model  
3.3. Implementation   
  
---
  
  
## Content

### 1. Streaming Data theory  

#### 1.1 What is streaming data

#### Real time vs Streaming processing vs Batch processing
streaming data: continous data (never ending-infinite), order, 
immutable (increamenting only), rerun, not save before use, river vs lake


#### Challenges
scalability, ordering, fault recorvery, fault tolerance, resilence, latency/durability, 
consistancy    

  
#### 1.2  Modes of streaming data  

#### Stateless vs statefull

#### Windows (tumbling/sliding/session)

#### Event time vs processing time


#### 1.3. Examples
 * performance monitoring of the system/machines to prevent failures, 
      ivestigate security issues 
 * feeding statistical models in real time  
     (fraud detection, stock market, recommendation engines) 
 * reporting, analysis   
     (gps-airlines,cars, marketing/sales analytics, customer/user activity,
      log monitoging)  
 * data visualisation  
 * movies, music
   
   
#### 2. Streaming  

#### 2.1 Protocols

TCP: low-level, bi-directional, full-duplex, and guaranteed order transport layer. No browser support (except via plugin/Flash).  
HTTP 1.0: request-response transport protocol layered on TCP. The client makes one full request, the server gives one full response, and then the connection is closed. The request methods (GET, POST, HEAD) have specific transactional meaning for resources on the server.  
HTTP 1.1: maintains the request-response nature of HTTP 1.0, but allows the connection to stay open for multiple full requests/full responses (one response per request). Still has full headers in the request and response but the connection is re-used and not closed. HTTP 1.1 also added some additional request methods (OPTIONS, PUT, DELETE, TRACE, CONNECT) which also have specific transactional meanings. However, as noted in the introduction to the HTTP 2.0 draft proposal, HTTP 1.1 pipelining is not widely deployed so this greatly limits the utility of HTTP 1.1 to solve latency between browsers and servers.  
Long-poll: sort of a "hack" to HTTP (either 1.0 or 1.1) where the server does not respond immediately (or only responds partially with headers) to the client request. After a server response, the client immediately sends a new request (using the same connection if over HTTP 1.1).  
HTTP streaming: a variety of techniques (multipart/chunked response) that allow the server to send more than one response to a single client request. The W3C is standardizing this as Server-Sent Events using a text/event-stream MIME type. The browser API (which is fairly similar to the WebSocket API) is called the EventSource API.  
Comet/server push: this is an umbrella term that includes both long-poll and HTTP streaming. Comet libraries usually support multiple techniques to try and maximize cross-browser and cross-server support.  
WebSockets: a transport layer built-on TCP that uses an HTTP friendly Upgrade handshake. Unlike TCP, which is a streaming transport, WebSockets is a message based transport: messages are delimited on the wire and are re-assembled in-full before delivery to the application. WebSocket connections are bi-directional, full-duplex and long-lived. After the initial handshake request/response, there is no transactional semantics and there is very little per message overhead. The client and server may send messages at any time and must handle message receipt asynchronously.  
SPDY: a Google initiated proposal to extend HTTP using a more efficient wire protocol but maintaining all HTTP semantics (request/response, cookies, encoding). SPDY introduces a new framing format (with length-prefixed frames) and specifies a way to layering HTTP request/response pairs onto the new framing layer. Headers can be compressed and new headers can be sent after the connection has been established. There are real world implementations of SPDY in browsers and servers.  
HTTP 2.0: has similar goals to SPDY: reduce HTTP latency and overhead while preserving HTTP semantics. The current draft is derived from SPDY and defines an upgrade handshake and data framing that is very similar the the WebSocket standard for handshake and framing. An alternate HTTP 2.0 draft proposal (httpbis-speed-mobility) actually uses WebSockets for the transport layer and adds the SPDY multiplexing and HTTP mapping as an WebSocket extension (WebSocket extensions are negotiated during the handshake).  
WebRTC/CU-WebRTC: proposals to allow peer-to-peer connectivity between browsers. This may enable lower average and maximum latency communication because as the underlying transport is SDP/datagram rather than TCP. This allows out-of-order delivery of packets/messages which avoids the TCP issue of latency spikes caused by dropped packets which delay delivery of all subsequent packets (to guarantee in-order delivery).  
QUIC: is an experimental protocol aimed at reducing web latency over that of TCP. On the surface, QUIC is very similar to TCP+TLS+SPDY implemented on UDP. QUIC provides multiplexing and flow control equivalent to HTTP/2, security equivalent to TLS, and connection semantics, reliability, and congestion control equivalentto TCP. Because TCP is implemented in operating system kernels, and middlebox firmware, making significant changes to TCP is next to impossible. However, since QUIC is built on top of UDP, it suffers from no such limitations. QUIC is designed and optimised for HTTP/2 semantics.  https://stackoverflow.com/questions/14703627/websockets-protocol-vs-http  

Websockets Spring:  
Simple Broker should delegate to  
recorvery  
disconections  

#### 2.2 Tools

Load balancing:  
 - NginX (IP-based)  
 - Apache HTTPD (cookie-based)  
 - HAProxy (cookie-based)  
 - Traefik (cookie-based)  
 - Node.js cluster module  
 
Kafka to client via websockets:
 - transfers_websockets_service  
 - kafka-websocket   
 - kafka-proxy-ws  
 
 
  
...
  
#### 3. Project
  
#### 3.1. About  
  
There is no business need behind the project.  
Just want to practice streaming data by using the most important issues like:  
stateful processing, event time, windows, streams updating, joining, filtering, splitting.  
In my opinion the best use cases to do such exercise is to make app based on time  
series like `application ploting return from investment wallet in near real time`.  
Then the result would be a computation of 2 merged time series streams:  
fx rates and financial instrument. Information about users' wallet could be  
stored in cache and updated from database.
  
Alternatives.  
   
#### 3.2. Model
  
General:  
![schema](https://bitbucket.org/woblak/doc/raw/3f4bd62bc141cb60ee32d236a5c893e48e57f693/static/process-general.png)  
  
---  
  
With details:  
![schema](https://bitbucket.org/woblak/doc/raw/3f4bd62bc141cb60ee32d236a5c893e48e57f693/static/process.png)  
  
---  
  
Db schema:  
![schema](https://bitbucket.org/woblak/doc/raw/3f4bd62bc141cb60ee32d236a5c893e48e57f693/static/db.png)  
  
---  
   
Scaling GET process schema:  
![schema](https://bitbucket.org/woblak/doc/raw/e4643752fb31d5e59c5da9d2b7e3f9b3032cccc9/static/process-details-get.png)  
  
---  
    
	
The system can be horizontally scaled out.   
 - Data emitters (fetchers) consist of: small jars with Tomcat embedded paired with yaml configuration files. Each emitter (fetcher) can emit (fetch) the subgroup of whole data. 
 For each crypto currency you can dedicated emitter. Just run same jar with another yaml configuration.   
 - Each emitter can have added replica which do the same. Data will be joined later. You can add as many replicas as you want. Just run same jar with another yaml configuration.       
 - Streaming data is made on Kafka which offers stable partitioning.  
 - Database made on TimescaleDB (PostgreSQL) can be tuned to use partitioning on each table.  
 - Results will be provided to gui by Websockets connections (or SSE). The work will be handled by many services managed by a load balancer. You can add more services. Just run same jar with another yaml configuration.  
   
#### 3.3. Implementation

#### technology stack:   

**backend:** Java 14, Spring, Bash  
**frontend:** Javascript, React  
**db:** Amazon Timescale  
**data processing:** Apache Kafka, Apache Kafka Streams  
**load balancing:** NGINX  
**tools:** Apache Maven, Docker  
**CI:**: bitbucket, jfrog  


---
  
  
## Literature
  
### Books:
Kafka - the definitive guide   
Kafka Streaming in action  
Stream processing with Apache Spark  
Streaming Data: Understanding the real-time pipeline  
  
### Internet:
  
#### Papers:
 

General:  
https://semver.org/  
http://tutorials.jenkov.com/data-streaming/index.html  
https://softwaremill.com/mqperf/  
https://www.oreilly.com/radar/the-world-beyond-batch-streaming-102/  
https://beam.apache.org/documentation/programming-guide/#windowing  
  


Kafka Streams:  
https://kafka.apache.org/documentation/  
https://kafka.apache.org/protocol  
https://docs.confluent.io/platform/current/streams/index.html  
https://kafka-tutorials.confluent.io  
https://www.confluent.io/blog/5-things-every-kafka-developer-should-know/  
https://www.confluent.io/blog/kafka-client-cannot-connect-to-broker-on-aws-on-docker-etc/  
  
  

Amazon Kinesis:  
https://d0.awsstatic.com/whitepapers/whitepaper-streaming-data-solutions-on-aws-with-amazon-kinesis.pdf  
  

Spark (Structured) Streaming:  
http://vishnuviswanath.com/spark_structured_streaming.html
https://jaceklaskowski.github.io/spark-structured-streaming-book/
https://cloudblogs.microsoft.com/opensource/2018/07/09/how-to-data-processing-apache-kafka-spark/  
http://spark.apache.org/docs/latest/structured-streaming-programming-guide.html  
https://spark.apache.org/docs/latest/streaming-programming-guide.html  
https://databricks.com/blog/2017/05/08/event-time-aggregation-watermarking-apache-sparks-structured-streaming.html  
https://www.adaltas.com/en/2019/04/18/spark-streaming-data-pipelines-with-structured-streaming/  
  

Visualistaion:  
http://holoviews.org/user_guide/Streaming_Data.html  
  

Web Sockets and Server Sent Events:  
https://tools.ietf.org/html/rfc6455  
https://www.sciencedirect.com/science/article/pii/S1877050919303576?via%3Dihub  
https://docs.spring.io/spring-framework/docs/current/reference/html/web.html#websocket  
https://stomp.github.io/stomp-specification-1.2.html  
https://centrifugal.github.io/centrifugo/blog/scaling_websocket/  
https://centrifugal.github.io/centrifugo/blog/scaling_websocket/  
https://socket.io/docs/v3/using-multiple-nodes/index.html  
https://blog.jscrambler.com/scaling-node-js-socket-server-with-nginx-and-redis/  
https://en.wikipedia.org/wiki/C10k_problem  
https://ably.com/topic/websockets-kafka  
https://dotsandbrackets.com/when-sse-is-much-better-choice-than-websocket/  
https://www.diva-portal.org/smash/get/diva2:1133465/FULLTEXT01.pdf  
https://technicalsand.com/spring-server-sent-events/  
  

Spring:  
https://docs.spring.io/spring-kafka/docs/current/reference/html/  
https://docs.spring.io/spring-integration/docs/current/reference/html/  
https://docs.spring.io/spring-cloud-stream-binder-kafka/docs/current/reference/html/spring-cloud-stream-binder-kafka.html#_kafka_streams_binder  

  
#### Presentations:  
https://www.youtube.com/watch?v=qYEvgtQLxqk  
https://www.youtube.com/watch?v=p3ScERQu9HY  
https://www.youtube.com/watch?v=4WjQ4l2pqsE  
https://www.youtube.com/watch?time_continue=3309&v=pJLtPpcv8bg&feature=emb_title  
https://www.youtube.com/watch?time_continue=1&v=sq-2MrnhxtA&feature=emb_title  
https://www.youtube.com/watch?v=sq-2MrnhxtA  

