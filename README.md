# Crypto Stream


## About  
Near real time crypto currency prices and volume visualisation with notifications.  

Changes are tested in Continous Delivery manner and published on every merge with master branch to: https://cryptostream.jfrog.io/artifactory/default-maven-virtual/com/woblak/cstream/ 

## Architecture  
![cstream-basic](https://bitbucket.org/woblak/doc/raw/942393cffb2fe089c5fb39cbbc49bb25b713dec8/static/cstream-basic.png) 
  
## Streaming topology  
![cstream-topology](https://bitbucket.org/woblak/doc/raw/942393cffb2fe089c5fb39cbbc49bb25b713dec8/static/cstream-topology.png)  